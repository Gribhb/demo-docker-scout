# Template for Docker Scout demo service

A repository containing an application and Dockerfile to demonstrate the use of Docker Scout Template to analyze and remediate CVEs in a container image. This is related to my [blog post]([Title](https://blog.antoinemayer.fr/2024/04/23/a-la-decouverte-de-docker-scout/))

To use this template, you need to define the following variables : 

* `DOCKER_HUB_PAT`
* `DOCKER_HUB_USER`

And then, you can adapte the variables value define in the [.gitlab-ci.yml](https://gitlab.com/Gribhb/demo-docker-scout/-/blob/poc-ci/.gitlab-ci.yml)

The application consists of a basic ExpressJS server and uses an intentionally old version of Express and Alpine base image.
 
 